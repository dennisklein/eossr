eossr.metadata package
======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   eossr.metadata.schema

Submodules
----------

eossr.metadata.codemeta module
------------------------------

.. automodule:: eossr.metadata.codemeta
   :members:
   :undoc-members:
   :show-inheritance:

eossr.metadata.codemeta2zenodo module
-------------------------------------

.. automodule:: eossr.metadata.codemeta2zenodo
   :members:
   :undoc-members:
   :show-inheritance:

eossr.metadata.zenodo module
----------------------------

.. automodule:: eossr.metadata.zenodo
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: eossr.metadata
   :members:
   :undoc-members:
   :show-inheritance:
