eossr.api package
=================

.. toctree::
   :maxdepth: 4

   eossr.api.zenodo


eossr.api.ossr module
---------------------

.. automodule:: eossr.api.ossr
   :members:
   :undoc-members:
   :show-inheritance:
