.. eossr documentation master file, created by
   sphinx-quickstart on Wed Sep  8 16:24:23 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. include:: README.md
   :parser: myst_parser.sphinx_


Documentation table of contents
===============================

.. toctree::
   :maxdepth: 3
   :glob:

   README
   docstring
   metadata
   notebooks/ossr_statistics.ipynb
   examples
   gitlab_to_zenodo
   snippets
   resources


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
