from .ossr import get_ossr_records  # noqa
from .zenodo import Record, get_zenodo_records  # noqa
